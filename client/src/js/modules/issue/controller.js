angular.module('laboard-frontend')
    .controller('IssueController', [
        '$scope', '$rootScope', 'Restangular', 'ColumnsRepository', '$modal', 'IssuesRepository', 'AuthorizationFactory',
        function($scope, $rootScope, Restangular, ColumnsRepository, $modal, $issues, $authorization) {
            $scope.close = function() {
                $issues.close($scope.issue);
            };

            $scope.unpin = function() {
                $scope.issue.from = $scope.column.title;
                $scope.issue.to = null;

                $issues.move($scope.issue)
            };

            $scope.drag = function() {
                $scope.issue.from = $scope.column;
                $scope.issue.column = null;
            };

            $scope.theme = function(theme) {
                $scope.issue.before = $scope.issue.theme || null;

                if (theme === $scope.issue.before) {
                    theme = null;
                }

                $scope.issue.after = theme;

                $issues.theme($scope.issue).then(null, function() {
                    $scope.issue.theme = $scope.issue.before;
                });
            };

            $scope.star = function(starred) {
                $scope.issue.starred = typeof starred === 'undefined' ? true : !!starred;

                $issues.star($scope.issue).then(null, function() {
                    $scope.issue.starred = !$scope.issue.starred;
                });
            };

            $scope.assign = function() {
                var issue = $scope.issue;

                $modal
                    .open({
                        templateUrl: 'issue/partials/assign.html',
                        controller: function($scope, $modalInstance) {
                            $scope.issue = issue;
                            $scope.assignTo = function(member) {
                                $modalInstance.close(member);
                            };
                        }
                    }).result
                        .then(function(member) {
                            $scope.issue.assignee = member;

                            $issues.persist($scope.issue);
                        });
            };

            $scope.zoom = function() {
                if ($scope.zooming) {
                    return;
                }

                $scope.zooming = true;

                $modal
                    .open({
                        size: 'lg',
                        templateUrl: 'issue/partials/modal.html',
                        resolve: {
                            issue: function() {
                                if ($scope.issue.description) {
                                    return $scope.issue;
                                }

                                return $issues.one($scope.issue.id);
                            }
                        },
                        controller: function($scope, $modalInstance, issue) {
                            $scope.issue = issue;
                        }
                    })
                    .opened.then(function() {
                        $scope.zooming = false;
                    })
            };

            $scope.draggable = $authorization.authorize('developer');
        }
    ]);
